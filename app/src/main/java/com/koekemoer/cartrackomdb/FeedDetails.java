package com.koekemoer.cartrackomdb;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FeedDetails extends AppCompatActivity {

    private TextView txtTitle, txtYear, txtDirector, txtLanguage, txtCountry;
    private ImageView imgPoster;
    SharedPreferences prefs;
    public static final String MyPREFERENCES = "MyPrefs" ;
    String ID;
    RequestQueue queue;
    String api_key_omdb;
    ProgressBar progressBarDetails;
    ActionBar actionBar;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_feed_details);

        //shared preferemnces for selected imdb item
        prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
        ID = prefs.getString("ID", "0");

        editor = prefs.edit();

        //api key get from strings
        api_key_omdb = getString(R.string.api_key_omdb);

        //progress bar
        progressBarDetails = findViewById(R.id.progressBarDetails);


        txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtYear = (TextView) findViewById(R.id.txtYear);
        imgPoster = (ImageView) findViewById(R.id.imgPoster);
        txtDirector = (TextView) findViewById(R.id.txtDirector);
        txtLanguage = (TextView) findViewById(R.id.txtLanguage);
        txtCountry = (TextView) findViewById(R.id.txtCountry);

        loadDetails();

        //clear title ID and close activity
        ImageButton button = (ImageButton) findViewById(R.id.imageButton);
        button.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                editor.putString("ID", "");
                editor.commit();
                finish();

            }
        });

    }
    @Override
    public void onBackPressed() {
        editor.putString("ID", "");
        editor.commit();
        finish();
    }


    public void loadDetails()
    {

        //load item json details

        String JsonURL = "https://www.omdbapi.com/?apikey="+api_key_omdb+"&i="+ID;

        queue = Volley.newRequestQueue(this);
        StringRequest request = new StringRequest(Request.Method.GET, JsonURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    //json response

                    JSONObject object=new JSONObject(response);

                    //set UI item text
                    txtTitle.setText(object.getString("Title"));
                    txtYear.setText(object.getString("Year"));
                    Picasso.get().load(object.getString("Poster")).into(imgPoster);
                    txtDirector.setText("Director: " + object.getString("Director"));
                    txtLanguage.setText("Language: " + object.getString("Language"));
                    txtCountry.setText("Country: " + object.getString("Country"));


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressBarDetails.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("error",error.toString());
            }
        });
        queue.add(request);

    }

}