package com.koekemoer.cartrackomdb;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.collection.ArraySet;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.koekemoer.cartrackomdb.models.item;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

// Kevin Koekemoer CarTrack Android Tech Challenge
public class MainActivity extends FeedDetails {

    private RadioGroup radioGroup;
    private EditText editTextSearch;
    private ListView listView;
    private FeedList feedView;
    private ProgressBar progressBar;
    private SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    String api_key_omdb;
    item it;
    List<item> rowItems;

    public static final String MyPREFERENCES = "MyPrefs" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //set theme
        setTheme(R.style.Theme_CarTrackOMDb);

        //shared preferemnces for selected imdb item
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        sharedpreferences = getSharedPreferences("MyPrefs", MODE_PRIVATE);
        if(!sharedpreferences.getString("ID", "0").equals(""))
        {
            //if item was open previously forward to details page
            Intent intent = new Intent(getApplicationContext(), FeedDetails.class);
            startActivity(intent);
        }

        //api key get from strings
        api_key_omdb = getString(R.string.api_key_omdb);

        listView=findViewById(R.id.listView);

        rowItems = new ArrayList<item>();

        //progress bar
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        //radio group media type
        radioGroup = (RadioGroup)findViewById(R.id.radioButtonGroupType);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                loadlist();
            }
        });

        //search text input
        editTextSearch = (EditText) findViewById(R.id.editTextSearch);
        editTextSearch.requestFocus();

        editTextSearch.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                loadlist();
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

    }

    public void loadlist() {

        progressBar.setVisibility(View.VISIBLE);

        String searchText = editTextSearch.getText().toString();

        //radio button get selected item
        int radioButtonID = radioGroup.getCheckedRadioButtonId();
        View radioButton = radioGroup.findViewById(radioButtonID);
        int idx = radioGroup.indexOfChild(radioButton);
        RadioButton r = (RadioButton) radioGroup.getChildAt(idx);
        String selectedtext = r.getText().toString();

        //json get async
        String JsonURL = "https://www.omdbapi.com/?apikey="+api_key_omdb+"&type="+selectedtext+"&s="+searchText;

        RequestQueue requestQueue;

        requestQueue = Volley.newRequestQueue(this);

        JsonObjectRequest obreq = new JsonObjectRequest(Request.Method.GET,
                JsonURL, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    //json response
                    JSONArray feedArray = response.getJSONArray("Search");

                    //setup listview object
                    it = null;
                    rowItems.clear();
                    for (int i = 0; i < feedArray.length(); i++) {
                        JSONObject feedObj = (JSONObject) feedArray.get(i);

                        it = new item(feedObj.getString("Title"), feedObj.getString("Year"), feedObj.getString("Poster"), feedObj.getString("imdbID"));
                        rowItems.add(it);
                    }

                    // Populating list data
                    feedView = new FeedList(MainActivity.this, rowItems);

                    //set listview custom adapter
                    listView.setAdapter(feedView);

                    //listview item onclick
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                            if(!String.valueOf(rowItems.get(position).getimdbID()).equals(""))
                            {
                                editor.putString("ID", String.valueOf(rowItems.get(position).getimdbID()));
                                editor.commit();

                                //open details page
                                Intent intent = new Intent(getApplicationContext(), FeedDetails.class);
                                startActivity(intent);
                            }

                        }
                    });

                } catch (JSONException e) {
                    //no results found show error list row
                    editor.putString("ID", "");

                    it = new item("No results found", "", "https://ia.media-imdb.com/images/M/MV5BMTU0MTE4Njg3Nl5BMl5BcG5nXkFtZTgwODY0NjQ2OTE@._V1_.png","");
                    rowItems.clear();
                    rowItems.add(it);
                    feedView = new FeedList(MainActivity.this, rowItems);
                    listView.setAdapter(feedView);
                }

                progressBar.setVisibility(View.GONE);
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Volley", "Error");
                    }
                }
        );

        requestQueue.add(obreq);
    }


}