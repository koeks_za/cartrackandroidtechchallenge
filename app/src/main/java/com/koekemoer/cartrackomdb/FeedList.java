package com.koekemoer.cartrackomdb;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.koekemoer.cartrackomdb.models.item;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;


public class FeedList extends ArrayAdapter {
    private String[] Title;
    private String[] Desc;
    private String[] image;
    private Activity context;

    public FeedList(Activity context, List<item> items) {
        super(context, R.layout.row_item, items);
        this.context = context;
        this.Title = Title;
        this.Desc = Desc;
        this.image = image;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        item rowItem = (item) getItem(position);
        View row=convertView;
        LayoutInflater inflater = context.getLayoutInflater();
        if(convertView==null)
            row = inflater.inflate(R.layout.row_item, null, true);
        TextView textViewTitle = (TextView) row.findViewById(R.id.textViewTitle);
        TextView textViewDesc = (TextView) row.findViewById(R.id.textViewDesc);
        ImageView imageFlag = (ImageView) row.findViewById(R.id.imageViewFlag);

        textViewTitle.setText(rowItem.getTitle());
        textViewDesc.setText(rowItem.getDesc());
        Picasso.get().load(rowItem.getImage()).into(imageFlag);

        return  row;
    }
}