package com.koekemoer.cartrackomdb.models;

public class item {

    private String Title;
    private String Desc;
    private String Image;
    private String imdbID;


    public item(String Title, String Desc, String Image, String imdbID) {
        this.Title = Title;
        this.Desc = Desc;
        this.Image = Image;
        this.imdbID = imdbID;
    }

    public static void add(item it) {
    }

    public String getTitle() {
        return Title;
    }
    public void setTitle(String Title) {
        this.Title = Title;
    }
    public String getDesc() {
        return Desc;
    }
    public void setDesc(String Desc) {
        this.Desc = Desc;
    }
    public String getImage() {
        return Image;
    }
    public void setImage(String Image) {
        this.Image = Image;
    }
    public String getimdbID() {
        return imdbID;
    }
    public void setimdbID(String imdbID) {
        this.imdbID = imdbID;
    }

}
